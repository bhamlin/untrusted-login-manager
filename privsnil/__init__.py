
import random

HASH_ITER_COUNT = 2


def RandomInt(_=None):
    return random.randrange(256)


def RandomByte(_=None):
    return bytes([RandomInt()])


def RandomBytes(count):
    return bytes(map(RandomInt, range(count)))


def rehash(content: bytes, hash_func, hash_count=None) -> bytes:
    """Hashes a content multiple times with the supplied hash class.

    This should probably be entirely replaced with a PBKDF2 instance.

    Args:
        content (bytes): Content to be hashed.
        hash_func: Function that returns an item with callables .update() and .digest().
        hash_count: Iteration count for hashing.

    """
    if not hash_count:
        hash_count = HASH_ITER_COUNT

    output = content
    for _ in range(hash_count):
        hasher = hash_func()
        hasher.update(output)
        output = hasher.digest()
    return output


class PasshashGenerator:
    def __init__(self, passhash):
        self.passhash = passhash
        self.revhash = self.passhash[::-1]

        s_index = self.passhash[-1] % len(self.revhash) + 1
        e_index = s_index + len(self.revhash)

        self.sohash = (self.revhash + self.revhash)[s_index:e_index]

    def _generate_hash(self, itm, salt, i):
        itm.update(bytes([i]))
        itm.update(salt)
        return itm.digest()

    def _generate_stream(self, hash, salt, hash_func):
        output = set()

        hash = hash_func(hash)
        for i in range(256):
            output.add(self._generate_hash(hash.copy(), salt, i))

        return output

    def generatePasswordStream(self, salt, hash_func):
        return self._generate_stream(self.passhash, salt, hash_func)

    def generateFirstOrderStream(self, salt, hash_func):
        return self._generate_stream(self.revhash, salt, hash_func)

    def generateSecondOrderStream(self, salt, hash_func):
        return self._generate_stream(self.sohash, salt, hash_func)
