#!/usr/bin/python3

import hashlib
import privsnil

privsnil.HASH_ITER_COUNT = 2

password = 'aaaa'
password_hash = privsnil.rehash(password.encode(), hashlib.md5)

print(f'Using password: {password} ({password_hash.hex()})')

salt = privsnil.RandomBytes(16)
print(f'Using salt:     {salt.hex()}')

PG = privsnil.PasshashGenerator(password_hash)

pms = PG.generatePasswordStream(salt, hashlib.md5)
pfs = PG.generateFirstOrderStream(salt, hashlib.md5)
pss = PG.generateSecondOrderStream(salt, hashlib.md5)

# Tests

hash_test = hashlib.md5()
hash_test.update(password_hash)
hash_test.update(privsnil.RandomByte())
hash_test.update(salt)
pms_test = hash_test.digest()

hash_test = hashlib.md5()
hash_test.update(PG.revhash)
hash_test.update(privsnil.RandomByte())
hash_test.update(salt)
pfs_test = hash_test.digest()

hash_test = hashlib.md5()
hash_test.update(PG.sohash)
hash_test.update(privsnil.RandomByte())
hash_test.update(salt)
pss_test = hash_test.digest()

print('Tests')
print('- Testing random main hash in sets: ')
print(
    f'  - main:{pms_test in pms} - first:{pms_test in pfs} - second:{pms_test in pss}')
print('- Testing random first order hash in sets: ')
print(
    f'  - main:{pfs_test in pms} - first:{pfs_test in pfs} - second:{pfs_test in pss}')
print('- Testing random first order hash in sets: ')
print(
    f'  - main:{pss_test in pms} - first:{pss_test in pfs} - second:{pss_test in pss}')
